#!/bin/sh -e
export BOWTIE_INDEXES='indexes'

if [ "$1" = "test_at_build_time" ] ; then
    export PATH=.:$PATH
else
    pkg=bowtie
    if [ "$AUTOPKGTEST_TMP" = "" ] ; then
      AUTOPKGTEST_TMP=`mktemp -d /tmp/${pkg}-test.XXXXXX`
    fi
    mkdir -p $AUTOPKGTEST_TMP/tests
    cp -a debian/tests/example* $AUTOPKGTEST_TMP/tests
    cd $AUTOPKGTEST_TMP
    cp -a /usr/share/doc/bowtie/examples/indexes $AUTOPKGTEST_TMP
fi

check_result () {
    EDIFF=`diff -u tests/$1.out $1.out`
    if ! $EDIFF ; then
        echo "Error testing example"
        echo $EDIFF
        exit 1
    else
        echo "$1 OK"
    fi
}

bowtie -a -v 2 e_coli --suppress 1,5,6,7 -c ATGCATCATGCGCCAT				> example1.out
check_result example1
bowtie -k 3 -v 2 e_coli --suppress 1,5,6,7 -c ATGCATCATGCGCCAT				> example2.out
check_result example2
bowtie -k 6 -v 2 e_coli --suppress 1,5,6,7 -c ATGCATCATGCGCCAT				> example3.out
check_result example3
bowtie -v 2 e_coli --suppress 1,5,6,7 -c ATGCATCATGCGCCAT				> example4.out
check_result example4
bowtie -a --best -v 2 e_coli --suppress 1,5,6,7 -c ATGCATCATGCGCCAT			> example5.out
# Please read README.Debian or
#   https://lists.debian.org/debian-med/2014/09/msg00031.html
# to evaluate the result od this test.
# check_result example5
# echo "There is a known difference between the manual and the test result"
# diff -u tests/example5.out example5.out
check_result example5
# Disabling test as the output of this version of bowtie differs from the older versions and
# causes check_result to fail
#bowtie -a --best --strata -v 2 --suppress 1,5,6,7 e_coli -c ATGCATCATGCGCCAT		> example6.out
#check_result example6
# The "No alignments" string is the end of stderr output
bowtie -a -m 3 -v 2 e_coli -c ATGCATCATGCGCCAT						2>&1 | tee | tail -n1 > example7.out
check_result example7
bowtie -a -m 5 -v 2 e_coli --suppress 1,5,6,7 -c ATGCATCATGCGCCAT			> example8.out
check_result example8
# Disabling test as the output of this version of bowtie differs from the older versions and
# causes check_result to fail
bowtie -a -m 3 --best --strata -v 2 e_coli --suppress 1,5,6,7 -c ATGCATCATGCGCCAT	> example9.out
#check_result example9
